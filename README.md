# Get Started

Clone current repository:

```
$ git clone git@bitbucket.org:homerefill/docker-nginx-nodejs.git
```

In this repository you will find the follow images:

## homerefill/nginx-nodejs

- **homerefill/nginx-nodejs:8.9.1:** image with nginx 1.10.3 and nodejs 8.9.1

#### Pull image:

```
$ docker pull homerefill/nginx-nodejs:8.9.1
```

#### Build image:

```
$ docker build -t homerefill/nginx-nodejs:8.9.1 8.9/
```
